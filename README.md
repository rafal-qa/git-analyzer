# Git repository analysis

This is a working configuration of [jQAssistant](https://jqassistant.org)
with [git plugin](https://github.com/kontext-e/jqassistant-plugins/blob/master/git/src/main/asciidoc/git.adoc)
in order to analyze git history.

It uses Java 17 and the latest versions of mentioned libraries.

I had some issues running this tool (see e.g. `.mvn/jvm.config`), that's why I published this sample.

I was particularly interested in Temporal Coupling analysis (several files are changing together over time), so this README focuses on it.

## Setup

Set `git.directory` in `pom.xml`. It's a full path of `.git` directory of any project.

## Running

Git repository needs to be scanned. Also, run this command when you change `git.directory`.

```
./mvnw clean jqassistant:scan
```

Start server and open http://localhost:7474

```
./mvnw jqassistant:server
```

## Analysis

Read this detailed article with many ready to paste queries:
[Shadows Of The Past: Analysis Of Git Repositories](https://jqassistant.org/shadows-of-the-past-analysis-of-git-repositories/)

For Temporal Coupling analysis run both below queries

```
MATCH
  (c:Commit),
  (c)-[:CONTAINS_CHANGE]->()-[:MODIFIES]->(f1),
  (c)-[:CONTAINS_CHANGE]->()-[:MODIFIES]->(f2)
WHERE
  f1 <> f2
  and id(f1) < id(f2)
  and not c:Merge
WITH
  f1, f2, count(c) as commits 
MERGE
  (f1)-[coupling:HAS_TEMPORAL_COUPLING_TO]->(f2)
SET
  coupling.weight = commits
RETURN
  count(*)
```

```
MATCH
  (f1:File)-[coupling:HAS_TEMPORAL_COUPLING_TO]->(f2:File)
RETURN
  f1.relativePath, f2.relativePath, coupling.weight as weight
ORDER BY
  weight desc
```